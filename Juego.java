package juego;

import java.awt.Color;

import entorno.Entorno;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {

	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	private Pelota pelota;
	private Slider slider;
	private Slider slider2;
	
	public Juego() {
		// Inicializa el objeto entorno
		entorno = new Entorno(this, "Pseudo Pong", 800, 600);
		
		pelota = new Pelota(entorno.ancho()/2, entorno.alto()/2, 50, Color.RED);
		slider = new Slider(entorno.ancho()/2,entorno.alto() - 15 , 200, 30, 2);
		slider2 = new Slider(entorno.ancho()/2 ,15, 200, 30, 2);
		entorno.iniciar();
	}

	/**
	 * Durante el juego, el método tick() será ejecutado en cada instante y 
	 * por lo tanto es el método más importante de esta clase. Aquí se debe 
	 * actualizar el estado interno del juego para simular el paso del tiempo 
	 * (ver el enunciado del TP para mayor detalle).
	 */
	public void tick() {
		pelota.dibujar(entorno);
		slider.dibujar(entorno);
		slider2.dibujar(entorno);
		pelota.mover();
		
		if (entorno.estaPresionada('s')) {
			pelota.acelerar();
		}
		
		if (pelota.chocasteConEntorno(entorno)) {
			pelota.cambiarDeDireccion();
		}
		
		if (pelota.chocasteConSlider(slider)) {
			pelota.cambiarDeDireccion();
		}
		
		if (entorno.estaPresionada('h')) {
			slider.moverIzquierda();
		}
		
		if (entorno.estaPresionada('f')) {
			slider.moverDerecha();
		}
		if (pelota.chocasteConSlider(slider2)) {
			pelota.cambiarDeDireccion();
		}
		
		if (entorno.estaPresionada(entorno.TECLA_ARRIBA)) {
			slider2.moverIzquierda();
		}
		
		if (entorno.estaPresionada(entorno.TECLA_ABAJO)) {
			slider2.moverDerecha();
		}
	}
	
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}

}
