package juego;

import java.awt.Color;

import entorno.Entorno;

public class Pelota {
	
	private double x;
	private double y;
	private double diametro;
	private Color color;
	private double velocidad;
	private double angulo;
	
	public Pelota(double x, double y, double diametro, Color color) {
		this.x = x;
		this.y = y;
		this.diametro = diametro;
		this.color = color;
		this.velocidad = 1;
		this.angulo = - Math.PI / 4;
	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarCirculo(x, y, diametro, color);
	}

	public void mover() {
		x += velocidad * Math.cos(angulo);
		y += velocidad * Math.sin(angulo);
	}

	public boolean chocasteConEntorno(Entorno entorno) {
		return x < diametro/2 || x > entorno.ancho() - diametro/2 || y < diametro/2 || y> entorno.alto()-diametro/2;
	}

	public void cambiarDeDireccion() {
		angulo += Math.PI / 2;
	}

	public void acelerar() {
		velocidad += 0.1;
	}

	public boolean chocasteConSlider(Slider s) {
		return s.getX() - s.getAncho()/2 <= x && x <= s.getX() + s.getAncho()/2 && y + diametro/2 > s.getY() - s.getAlto()/2;
	}

}
