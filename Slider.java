package juego;

import java.awt.Color;

import entorno.Entorno;

public class Slider {

	private double x;
	private double y;
	private int ancho;
	private int alto;
	private double velocidad;
	private Color color;
	
	public Slider(double x, double y, int ancho, int alto, double velocidad) {
		this.x = x;
		this.y = y;
		this.ancho = ancho;
		this.alto = alto;
		this.velocidad = velocidad;
		this.color = Color.YELLOW;
	}
	
	public void dibujar(Entorno entorno) {
		entorno.dibujarRectangulo(x, y, ancho, alto, 0, color);
	}
	
	public void moverIzquierda() {
		x -= velocidad;
	}
	
	public void moverDerecha() {
		x += velocidad;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public int getAncho() {
		return ancho;
	}

	public int getAlto() {
		return alto;
	}
	
}
